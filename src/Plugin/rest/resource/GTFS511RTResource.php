<?php


namespace Drupal\gtfs_511\Plugin\rest\resource;

use Drupal;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Drupal\Core\Cache\CacheableMetadata;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Fetches new RT data
 *
 * @RestResource(
 *   id = "gtfs_511_rt_resource",
 *   label = @Translation("Fetch 511 GTFS RT data"),
 *   uri_paths = {
 *     "canonical" = "/gtfs/api/v1/511/rt/{api}/{agency}"
 *   }
 * )
 */
class GTFS511RTResource extends ResourceBase {

  /**
   * Responds to GET requests.
   *
   * @return \Drupal\gtfs\Plugin\rest\resource\ResourceResponse
   */
  public function get($api, $agency) {

    Drupal::service('page_cache_kill_switch')->trigger();

    (new Drupal\gtfs_511\Services\GTFS511Fetcher())->fetch();

    $response = gtfs_511_get_data($api, $agency);

    return JsonResponse::create($response);


    $disable_cache = new CacheableMetadata();
    $disable_cache->setCacheMaxAge(0);

    return (new ResourceResponse($response))->addCacheableDependency($disable_cache)
      ->addCacheableDependency($response);
  }

}
