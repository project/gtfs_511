<?php


namespace Drupal\gtfs_511\Plugin\rest\resource;

use Drupal;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Drupal\Core\Cache\CacheableMetadata;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Fetches new RT data
 *
 * @RestResource(
 *   id = "gtfs_511_stop_prediction_resource",
 *   label = @Translation("Fetch 511 GTFS RT data for a stop"),
 *   uri_paths = {
 *     "canonical" = "/gtfs/api/v1/511/rt/stops/{stop_id}"
 *   }
 * )
 */
class GTFS511StopPredictionResource extends ResourceBase {

  /**
   * Responds to GET requests.
   *
   * @return \Drupal\gtfs\Plugin\rest\resource\ResourceResponse
   */
  public function get($stop_id) {

    Drupal::service('page_cache_kill_switch')->trigger();

    (new Drupal\gtfs_511\Services\GTFS511Fetcher())->fetch();

    $stop = Drupal\gtfs\Entity\Stop::getById($stop_id);

    $response = array_values(array_map(function ($route) use ($stop) {
      $agency = $route->agency();
      $data = gtfs_511_get_data('tripupdates', $agency->get('field_511_id')->value);
      $predictions_for_stop = array_values(array_filter($data->Entities, function ($entity) use ($stop, $route) {
        $stop_ids_serviced = array_map(function ($StopTimeUpdate) {
          return $StopTimeUpdate->StopId;
        }, array_filter($entity->TripUpdate->StopTimeUpdates, function ($StopTimeUpdate) {
          // Filter on stoptimes that have either an arrival or departure
          return !!$StopTimeUpdate->Arrival || !!$StopTimeUpdate->Departure;
        }));
        return (
          in_array($stop->get('stop_id')->value, $stop_ids_serviced)
          && $entity->TripUpdate->Trip->RouteId === $route->get('route_short_name')->value
        );
      }));
      foreach ($predictions_for_stop as &$prediction) {
        $prediction->TripUpdate->StopTimeUpdates = array_values(array_filter($prediction->TripUpdate->StopTimeUpdates, function ($StopTimeUpdate) use ($stop) {
          return (
            (
              !!$StopTimeUpdate->Arrival
              || !!$StopTimeUpdate->Departure
            )
            && $StopTimeUpdate->StopId === $stop->get('stop_id')->value
          );
        }));
      }
      return [
        'route' => $route->toGTFSObject() + ['agency' => $route->agency()->toGTFSObject()],
        'predictions' => $predictions_for_stop,
      ];
    }, $stop->routes()));

    return JsonResponse::create($response);


    $disable_cache = new CacheableMetadata();
    $disable_cache->setCacheMaxAge(0);

    return (new ResourceResponse($response))->addCacheableDependency($disable_cache)
      ->addCacheableDependency($response);
  }

}
