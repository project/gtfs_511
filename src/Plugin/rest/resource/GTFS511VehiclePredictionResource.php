<?php


namespace Drupal\gtfs_511\Plugin\rest\resource;

use Drupal;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Drupal\Core\Cache\CacheableMetadata;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Fetches new RT data
 *
 * @RestResource(
 *   id = "gtfs_511_vehicle_prediction_resource",
 *   label = @Translation("Fetch 511 GTFS RT data for a vehicle"),
 *   uri_paths = {
 *     "canonical" = "/gtfs/api/v1/511/rt/{agency}/vehicles/{vehicle}"
 *   }
 * )
 */
class GTFS511VehiclePredictionResource extends ResourceBase {

  /**
   * Responds to GET requests.
   *
   * @return \Drupal\gtfs\Plugin\rest\resource\ResourceResponse
   */
  public function get($agency, $vehicle) {

    Drupal::service('page_cache_kill_switch')->trigger();

    (new Drupal\gtfs_511\Services\GTFS511Fetcher())->fetch();

    $tripupdates = gtfs_511_get_data('tripupdates', $agency);

    $response = array_values(array_filter($tripupdates->Entities, function ($entity) use ($vehicle) {
      return isset($entity->TripUpdate->Vehicle->Id) && $entity->TripUpdate->Vehicle->Id == $vehicle;
    }));

    return JsonResponse::create($response);


    $disable_cache = new CacheableMetadata();
    $disable_cache->setCacheMaxAge(0);

    return (new ResourceResponse($response))->addCacheableDependency($disable_cache)
      ->addCacheableDependency($response);
  }

}
