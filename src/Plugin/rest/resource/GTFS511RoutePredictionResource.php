<?php


namespace Drupal\gtfs_511\Plugin\rest\resource;

use Drupal;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Drupal\Core\Cache\CacheableMetadata;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Fetches new RT data
 *
 * @RestResource(
 *   id = "gtfs_511_route_prediction_resource",
 *   label = @Translation("Fetch 511 GTFS RT data for a route"),
 *   uri_paths = {
 *     "canonical" = "/gtfs/api/v1/511/rt/routes/{route_id}"
 *   }
 * )
 */
class GTFS511RoutePredictionResource extends ResourceBase {

  /**
   * Responds to GET requests.
   *
   * @return \Drupal\gtfs\Plugin\rest\resource\ResourceResponse
   */
  public function get($route_id) {

    Drupal::service('page_cache_kill_switch')->trigger();

    (new Drupal\gtfs_511\Services\GTFS511Fetcher())->fetch();

    $route = Drupal\gtfs\Entity\Route::load(\Drupal::database()->query(
      "SELECT `id`
      FROM {gtfs_route_field_data}
      WHERE `route_id` = :route_id",
      [':route_id' => $route_id]
    )->fetch(\PDO::FETCH_COLUMN));

    $agency = $route->agency();
    $data = gtfs_511_get_data('tripupdates', $agency->get('field_511_id')->value);
    $predictions_for_route = array_values(array_filter($data->Entities, function ($entity) use ($route) {
      return $entity->TripUpdate->Trip->RouteId === $route->get('route_short_name')->value;
    }));
    foreach ($predictions_for_route as &$prediction) {
      $prediction->TripUpdate->StopTimeUpdates = array_values(array_filter($prediction->TripUpdate->StopTimeUpdates, function ($StopTimeUpdate) {
        return !!$StopTimeUpdate->Arrival || !!$StopTimeUpdate->Departure;
      }));
    }
    $response = [
      'route' => $route->toGTFSObject() + ['agency' => $route->agency()->toGTFSObject()],
      'predictions' => $predictions_for_route
    ];

    return JsonResponse::create($response);


    $disable_cache = new CacheableMetadata();
    $disable_cache->setCacheMaxAge(0);

    return (new ResourceResponse($response))->addCacheableDependency($disable_cache)
      ->addCacheableDependency($response);
  }

}
