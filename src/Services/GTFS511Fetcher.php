<?php

namespace Drupal\gtfs_511\Services;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Psr7\Request;

class GTFS511Fetcher {
  public function fetch() {
    $refresh_interval = 60;
    $last_fetch = \Drupal::state()->get('gtfs_511_rt.last_fetch', 0);
    $config = \Drupal::config(\Drupal\gtfs_511\Form\GTFS511ConfigForm::SETTINGS);
    if (!(time() >= $last_fetch + $refresh_interval)) {
      return;
    }
    if (!\Drupal::lock()->acquire('gtfs_511_rt_fetch', $refresh_interval)) {
      return;
    }

    $apis = ['tripupdates', 'vehiclepositions', 'servicealerts'];
    $agencies = gtfs_511_agencies();
    $api_key = $config->get('api_key');
    $db = \Drupal::database();
    $client = new Client();

    foreach ($apis as $api) {
      foreach ($agencies as $agency) {
        $url = "https://api.511.org/transit/{$api}?api_key={$api_key}&agency={$agency}&format=json";
        try {
          $response = $client->request('GET', $url);
        } catch (GuzzleException $e) {
          \Drupal::logger('gtfs_511_rt')->error($e->getMessage());
        }

        if (!isset($response)) continue;
        $contents = $response->getBody();
        $db
          ->merge("gtfs_511_{$api}")
          ->key('agency', $agency)
          ->fields([
            'timestamp' => time(),
            'data' => (string) $contents
          ])
          ->execute();
      }
    }

    \Drupal::lock()->release('gtfs_511_rt_fetch');
    \Drupal::state()->set('gtfs_511_rt.last_fetch', time());
  }
}